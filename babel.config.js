module.exports = function (api) {
  api.cache(true);

  const presets = [
    [
      "@vue/app",
      {
        "targets": {
          "ie": "11"
        }
      }
    ]
  ];
  const plugins = [];

  return {
    presets,
    plugins
  };
}