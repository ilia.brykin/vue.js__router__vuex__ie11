import Vue from "vue";
import Vuex from "vuex";
import _ from "lodash";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    ME: undefined,
    STATUS_SIGNIN: undefined,
    NOTIFICATION: {},
    NOTIFICATION_COUNT: 0,
  },
  mutations: {
    ADD_NOTIFICATION_MUT(state, item) {
      const notificationClone = _.cloneDeep(state.NOTIFICATION);
      notificationClone[state.NOTIFICATION_COUNT] = item;
      state.NOTIFICATION = notificationClone;
      state.NOTIFICATION_COUNT++;
    },

    REMOVE_NOTIFICATION_MUT(state, index) {
      const notificationClone = _.cloneDeep(state.NOTIFICATION);
      if (notificationClone[index]) {
        delete notificationClone[index];
      }
      state.NOTIFICATION = notificationClone;
    },

    ME_MUT(state, me) {
      state.ME = me;
    },

    UPDATE_ME_MUT(state, meResponse) {
      state.ME = _.assign({}, state.ME, meResponse);
    },

    STATUS_SIGNIN_MUT(state, me) {
      state.ME = me;
    },
  },
  actions: {
    ADD_NOTIFICATION_ACT({ commit, state }, message) {
      const currentIndex = state.NOTIFICATION_COUNT;
      commit("ADD_NOTIFICATION_MUT", message);
      setTimeout(() => {
        commit("REMOVE_NOTIFICATION_MUT", currentIndex);
      }, 5000);
    },
  }
})
