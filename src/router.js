import Vue from "vue";
import Router from "vue-router";

import Start from "./views/Private/Start/Start.vue";

function setTitle(to) {
  if (to.meta && to.meta.title) {
    document.title = to.meta.title;
  }
}

Vue.use(Router);


const router = new Router({
  base: "/",
  mode: "history",
  routes: [
    {
      path: "/",
      name: "start",
      component: Start,
      meta: {
        title: "Start",
      },
    },
    { path: "*",
      name: "rest",
      component: Start,
    }
  ]
});

router.beforeEach((to, from, next) => {
  setTitle(to);
  next();
});

export default router;
