var path = require('path')
var CopyWebpackPlugin = require('copy-webpack-plugin');
const { VueLoaderPlugin } = require('vue-loader');

function resolveAlias(mode) {
  return {
    "vue": path.resolve(__dirname, mode === "development" ? "node_modules/vue/dist/vue.js" : "node_modules/vue/dist/vue.min.js"),
  };
};

module.exports = (env, options) => {
  return {
    entry: "./src/main.js",
    output: {
      path: path.resolve(__dirname, "./dist/"),
      publicPath: "/dist/",
      filename: "build.js",
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          enforce: "pre",
          use: [
            {
              loader: "eslint-loader"
            }

          ],
          exclude: /node_modules/
        },
        {
          test: /\.pug$/,
          loader: 'pug-plain-loader'
        },

        {
          test: /\.vue$/,
          loader: 'vue-loader',
          options: {
            loaders: {
              js: [
                {
                  loader: resolveAlias["eslint-loader"],
                  options: {
                    formatter: require('eslint-friendly-formatter'),
                  },
                }
              ]
            }
          }
        },
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: "babel-loader",
        },
        {
          test: /\.scss$/,
          exclude: /node_modules/,
          use: [
            {
              loader: "style-loader"
            },
            {
              loader: "css?sourceMap"
            },
            {
              loader: "sass?sourceMap"
            }
          ],
        },
      ]
    },
    plugins: [
      new VueLoaderPlugin(),
      new CopyWebpackPlugin([
        { from: './assets', to: 'assets' },
      ])
    ],
    resolve: {
      alias: resolveAlias(options.mode)
    },
    devServer: {
      historyApiFallback: true
    },
  };
};
